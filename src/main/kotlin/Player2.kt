class Player2(_name: String, val health: Int) {
    val race = "Dwarf"
    var town = "Bavaria"
    val name = _name
    lateinit var alignment: String
    private var age = 0

    init {
        println("Initializing player")
        //alignment = "Good"
    }

    constructor(_name: String): this(_name, 100) {
        town = "The Shire"
    }

    fun determineFate(){
        alignment = "Good"
    }

    fun proclaimFate(){
        if (::alignment.isInitialized) println(alignment)
    }
}