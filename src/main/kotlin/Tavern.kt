import extensions.random as randomizer
import java.io.File
import kotlin.math.roundToInt

const val TAVERN_NAME = "Taernyl's Folly"


//var playerGold = 10
//var playerSilver = 10
val patronList = mutableListOf("Eli", "Mordoc", "Sophie")
val lastName = listOf("Ironfoot", "Fernsworth", "Baggins")
val uniquePatrons = mutableSetOf<String>()
val menuList = File("data/tavern-menu-items.txt")
    .readText()
    .split("\r\n")
val patronGold = mutableMapOf <String, Double>()

fun main() {
    printMenu()
    printMenu2()

    if (patronList.contains("Eli")) {
        println("The tavern master says: Eli's in the back playing cards.")
    } else {
        println("The tavern master says: Eli isn't here.")
    }
    if (patronList.containsAll(listOf("Mordoc", "Sophie"))) {
        println("The tavern master says: Yea, they're seated by the stew kettle.")
    } else {
        println("The tavern master says: Nay, they departed hours ago.")
    }

    //for (patron in patronList) println("Good evening, $patron")
    //patronList.forEach { patron -> println("Good evening, $patron") }
//    patronList.forEachIndexed { index, patron ->
//        println("Good evening, $patron, you are #${index + 1} in line")
//        placeOrder(patron, menuList.shuffled().first())
//    }

//    menuList.forEachIndexed { index, data -> println("$index : $data")  }
    //placeOrder("elixir,Shirley's Temple,4.12")

    (0..9).forEach{
        val first = patronList.randomizer()
        val last = lastName.randomizer()
        val name = "$first $last"
        uniquePatrons += name
    }
    uniquePatrons.forEach {
        patronGold[it] = 6.0 }

    var orderCount = 0
    while (orderCount < 9) {
        placeOrder(uniquePatrons.randomizer(), menuList.randomizer())
        orderCount ++
    }
    displayPatronBalances()
    checkingPatronBalances()
    println("=============")
    displayPatronBalances()
}

private fun checkingPatronBalances(){
    val removePatron = mutableSetOf<String>()
    patronGold.forEach { patron, balance ->
        if (balance <= 0.0) removePatron += patron
    }
    patronGold -= removePatron
    uniquePatrons -= removePatron
}

private fun displayPatronBalances(){
    patronGold.forEach { patron, balance ->
        println("$patron, balance: ${"%.2f".format(balance)}")
    }
}

/*fun performPurchase(price: Double) {
    displayBalance()
    val totalPurse = playerGold + playerSilver / 100.0
    println("Total purse: $totalPurse")
    println("Purchasing item for $price")
    val remainingBalance = totalPurse - price
    println("Remaining balance: ${"%.2f".format(remainingBalance)}")

    val remainingGold = remainingBalance.toInt()
    val remainingSilver = (remainingBalance % 1 * 100).roundToInt()
    playerGold = remainingGold
    playerSilver = remainingSilver
    displayBalance()
}*/

fun performPurchase2(price: Double, patronName: String){
    val totalPurse = patronGold.getValue(patronName)
    patronGold[patronName] = totalPurse - price
}

/*private fun displayBalance() {
    println("Player's purse balance: Gold: $playerGold, Silver: $playerSilver")
}*/

private fun placeOrder(patronName: String, menuData: String) {
    val indexOfApostrophe = TAVERN_NAME.indexOf('\'')
    val tavernMaster = TAVERN_NAME.substring(0 until indexOfApostrophe)
    println("$patronName speaks with $tavernMaster about their order.")
    val (type, name, price) = menuData.split(',')
    val message = "$patronName buys a $name ($type) for $price."
    println(message)

    performPurchase2(price.toDouble(), patronName)

    val phrase = if (name == "Dragon's Breath") {
        "$patronName exclaims: ${toDragonSpeak("Ah, delicious $name!")}"
    } else {
        "$patronName says: Thanks for the $name"
    }
    println(phrase)
}

// like extension
// private fun String.toDragonSpeak(phrase: String) =
// и тут как в функции.

private fun toDragonSpeak(phrase: String) =
    phrase.replace(Regex("[aeiouAEIOU]")) {
        when (it.value) {
            "a", "A" -> "4"
            "e", "E" -> "3"
            "i", "I" -> "1"
            "o", "O" -> "0"
            "u", "U" -> "|_|"
            else -> it.value
        }
    }



fun printMenu() {
    println()
    val hello = "*** Welcome to Taernyl's Folly ***"
    val cnt = hello.count()
    println(hello)
    println()
    menuList.forEach { item ->
        val (_, name, price) = item.split(',')
        val nameOut = name[0].uppercaseChar() + name.substring(1, name.count())
        val pos = price.indexOf('.')
        val priceOut = if (price.count() - pos == 2) {
            price + '0'
        } else {
            price
        }
        val s = nameOut.padEnd(cnt - priceOut.count(), '.')
        println(s + priceOut)
    }
    println()
}

fun printMenu2(){
    println()
    val hello = "*** Welcome to Taernyl's Folly ***"
    val cnt = hello.count()
    println(hello)
    val typeList = mutableListOf<String>()
    menuList.forEach { item ->
        val (type, _, _) = item.split(',')
        if (!typeList.contains(type)) {
            typeList.add(type)
            val titleType = "   ~ [" + type + "] ~   "
            println(titleType)
            menuList.forEach { t ->
                val (type2, name, price) = t.split(',')
                if (type == type2) {
                    val nameOut = name[0].uppercaseChar() + name.substring(1, name.count())
                    val pos = price.indexOf('.')
                    val priceOut = if (price.count() - pos == 2 ) {
                        price + '0'
                    } else {
                        price
                    }
                    val s = nameOut.padEnd(cnt - priceOut.count(), '.')
                    println(s + priceOut)
                }
            }
        }

    }
    println()
}

